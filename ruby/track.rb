class Track
	def initialize(track_data)
		@pieces = track_data['race']['track']['pieces'].map{ |piece_data| Piece.new piece_data }
		@lanes = track_data['race']['track']['lanes']
		@position = {}
		@path = 0
	end

	def set_lane(lane_radius)
		@pieces.each{ |piece| piece.set_lane lane_radius }
	end

	def path(position)
		@position = position
		set_lane @lanes[position['lane']['endLaneIndex']]['distanceFromCenter']
		@path = distance_to(position['pieceIndex']) + position['inPieceDistance']
	end

	def distance_to(index, from = 0)
		path = 0
		if index > 0
			@pieces[0..index-1].each{ |piece| path += piece.length }
		end
		path - from
	end

	def list_of_turns
		turns = []
		@pieces.each_with_index{ |piece, i| turns << i if piece.turn }
		turns
	end

	def max_speeds(fN)
		turns = []
		if @pieces[@position['pieceIndex']].turn
			piece = @pieces[@position['pieceIndex']]
			turns << {distance: 0, max_speed: piece.max_speed(fN), radius: piece.radius}
		end
		@pieces.each_with_index do |piece, i| 
			if piece.turn 
				distance = distance_to(i, @path)
				optimal_lane = piece.optimal_lane < 0 ? 0 : @lanes.length - 1
				turns << {distance: distance, max_speed: piece.max_speed(fN), optimal_lane: optimal_lane} if distance > 0
			end
		end
		turns
	end
end
