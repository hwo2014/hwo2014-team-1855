class Car

	attr_accessor :color

	def initialize(color)
		self.color = color

		@angle 	= 0
		@speed 	= 0
		@a0 	= -1
		@k  	= -1
		@fN 	= 0

		@max_speed  = -1
		@path 		= 0
		@throttle 	= 1
	end

	def update_position(car_data)
		@angle    = car_data['angle']
		@position = car_data['piecePosition']
		@lane 	  = car_data['piecePosition']['lane']['endLaneIndex']
	end

	def position
		@position
	end

	def set_path(path)
		speed  = path - @path
		if speed > 0 && @speed == 0 && @a0 < 0 
			@a0 = speed 
			puts "Found a0 to be #{@a0}"
		end
		if @speed > 0 && @k < 0
			@k = (@a0 - speed + @speed)/@speed 
			puts "Found k to be #{@k}"
		end
		@path  = path
		@speed = speed

		@max_speed = @a0/@k if @max_speed < 0 && @k > 0 && @a0 > 0
	end

	def throttle
		@throttle
	end

	def speed
		@speed
	end

	def angle
		@angle
	end

	def fN
		@fN
	end

	def  set_turns(turns)
		@turns = turns
	end

	def turning
		@turns.include? @position['pieceIndex']
	end

	def update_max_speeds(turns)
		prev_throttle = @throttle

		check_optimal_lane turns

		if turning
			@throttle = @speed < turns[0][:max_speed] ? 0.99 : [turns[0][:max_speed]/@max_speed, 0.99].min
			if @fN == 0 
				if @angle.abs < 1
					@throttle = 1
					puts "Finding friction constant" if prev_throttle != @throttle
				else
					@fN = @speed*@speed/turns[0][:radius]
					puts "Found friction constant to be #{@fN}"
				end
				return
			end
		else
			@throttle = 1
		end
		return if @a0 < 0 || @k < 0
		breaking = false
		turns.each do |turn|
			if @speed - turn[:max_speed] > 0 && turn[:distance] > 0
				turn[:max_speed] = @max_speed/10 if turn[:max_speed] == 0
				t = -Math.log(turn[:max_speed]/@speed)/@k
				s = (@speed*(1-Math.exp(-@k*t)))/@k
				if (s > turn[:distance])
					@throttle = 0
					puts "Breaking s: #{s} d: #{turn[:distance]} v: #{turn[:max_speed]}" if prev_throttle != @throttle
					breaking = true
					break
				end
			end
		end
		if !breaking && prev_throttle != @throttle 
			if @throttle == 1
				puts "Entering straight"
			else
				puts "Entering turn, max speed: #{turns[0][:max_speed]} speed: #{@speed}"
			end
		end
	end

	def check_optimal_lane(turns)
		turns.each do |turn|
			next if turn[:distance] == 0
			if turn[:optimal_lane] != @lane
				switch_at_next(turn[:optimal_lane] < @lane ? 'Left' : 'Right')
			end
			break
		end
	end

	def switch_at_next(to)
		if @switch_to.nil? || @switch_to != to
			self.switch_sent = false
			@switch_to   = to
		end
	end

	def switch_to
		@switch_to
	end

	attr_accessor :switch_sent

end
