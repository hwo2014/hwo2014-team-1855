class Bot
	def initialize(server_host, server_port, bot_name, bot_key)
		tcp = TCPSocket.open(server_host, server_port)
		@time = -1
		play bot_name, bot_key, tcp
	end

	private

	def play(bot_name, bot_key, tcp)
		tcp.puts join_message(bot_name, bot_key)
		react_to_messages_from_server tcp
	end

	def react_to_messages_from_server(tcp)
		counter = 0
		while json = tcp.gets
			message = JSON.parse(json)
			msgType = message['msgType']
			msgData = message['data']

			@time = message['gameTick'] unless message['gameTick'].nil?

			case msgType
				when 'join'
					puts 'Joined'
				when 'yourCar'
					puts 'My car'
					@car = Car.new msgData['color']
				when 'gameInit'
					puts 'Init game'
					@track = Track.new(msgData)
					@car.set_turns @track.list_of_turns
				when 'gameStart'
					puts 'Race started'
				when 'carPositions'
					@car.update_position msgData.find{ |car| car['id']['color'] == @car.color }
					@car.set_path @track.path(@car.position)
					@car.update_max_speeds @track.max_speeds(@car.fN)
				when 'gameEnd'
					puts 'Race ended'
				when 'error'
					puts "ERROR: #{msgData}"
				else
					puts "Got #{msgType}"
					pp msgData
			end

			if !@car.nil? && !@car.switch_sent.nil? && !@car.switch_sent
				tcp.puts make_msg('switchLane', @car.switch_to)
				@car.switch_sent = true
				puts "Switching lanes to #{@car.switch_to}"
			end

			if @time > -1
				puts "throttle #{@car.throttle} speed: #{@car.speed} angle: #{@car.angle}" if counter % 10 == 0
				counter += 1
				tcp.puts throttle_message(@car.throttle)
			end			
		end
	end

	def join_message(bot_name, bot_key)
		make_msg("join", {:name => bot_name, :key => bot_key})
	end

	def throttle_message(throttle)
		make_msg("throttle", throttle)
	end

	def ping_message
		make_msg("ping", {})
	end

	def make_msg(msgType, data)
		JSON.generate({:msgType => msgType, :data => data})
	end
end