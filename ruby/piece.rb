class Piece
	def initialize(piece_data)
		if !piece_data['length'].nil?
			@length = piece_data['length']
			@turn   = false
		else
			@radius = piece_data['radius']
			@angle  = piece_data['angle']
			@turn 	= true
			@optimal_lane = @angle > 0 ? 1 : -1
		end
		@switch = !piece_data['switch'].nil? && piece_data['switch']
	end

	def set_lane(lane_radius)
		return true if @lane_radius == lane_radius 
		@lane_radius = lane_radius
		@length = @angle.abs/360*2*Math::PI*radius if @turn
	end

	def radius
		@radius + (@angle > 0 ? -1 : 1)*@lane_radius
	end

	def length
		@length
	end

	def turn
		@turn
	end

	def max_speed(fN)
		Math.sqrt( fN*radius )
	end

	def optimal_lane
		@optimal_lane
	end

	def switch
		@switch
	end

end