require 'json'
require 'socket'
require 'pp'
require './bot.rb'
require './track.rb'
require './piece.rb'
require './car.rb'

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]

puts "#{bot_name} connecting to #{server_host}:#{server_port}"

Bot.new(server_host, server_port, bot_name, bot_key)